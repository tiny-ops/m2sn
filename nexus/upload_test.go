package nexus

import (
	"log"
	"net/http"
	"os"
	"testing"
)

func TestGetArtifactUploadJobsFromPath(t *testing.T) {
	logger := log.New(os.Stderr, "", log.LstdFlags)

	nexusRepoUrl := "https://nexus.company.com/repository/maven2"

	jobs := GetArtifactUploadJobsFromPath(nexusRepoUrl, "../test-data/artifacts", "", logger)

	if len(jobs) != 7 {
		t.Fatalf("unexpected amount of file upload jobs %d, expected: 7", len(jobs))
	}
}

// Integration test
func TestUploadArtifact(t *testing.T) {
	t.Skip()
	logger := log.New(os.Stderr, "", log.LstdFlags)

	client := &http.Client{}

	err := UploadArtifact(client, "CHANGE-ME", "CHANGE-ME", "CHANGE-ME", "CHANGE-ME", logger)

	if err != nil {
		t.Fatal(err.Error())
	}
}
