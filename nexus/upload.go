package nexus

import (
	"bytes"
	b64 "encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"m2sn/maven"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"strings"
)

type FileUploadJob struct {
	FilePath string
	Url      string
}

func UploadArtifact(client *http.Client, filePath string, targetUrl string, username string, password string, logger *log.Logger) error {
	logger.Printf("uploading artifact '%s' to '%s'..\n", filePath, targetUrl)

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	file, err := os.Open(filePath)
	defer file.Close()

	filePart, err := writer.CreateFormFile("", filePath)
	_, err = io.Copy(filePart, file)
	if err != nil {
		logger.Println(err.Error())
		return err
	}
	err = writer.Close()
	if err != nil {
		logger.Println(err.Error())
		return err
	}

	req, err := http.NewRequest("PUT", targetUrl, payload)

	if err != nil {
		logger.Println(err.Error())
		return err
	}

	encodedCreds := b64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", username, password)))

	req.Header.Add("Authorization", fmt.Sprintf("Basic %s", encodedCreds))

	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := client.Do(req)
	if err != nil {
		logger.Println(err.Error())
		return err
	}
	defer res.Body.Close()

	logger.Println("server response status:", res.Status)

	if res.StatusCode == http.StatusCreated {
		return nil

	} else {
		return errors.New("unexpected server response")
	}
}

func GetArtifactUploadJobsFromPath(nexusRepoUrl string, basePath string, urlContinueMask string, logger *log.Logger) []FileUploadJob {
	logger.Printf("get file upload jobs from path '%s'..\n", basePath)
	urls := make([]FileUploadJob, 0)

	items, _ := os.ReadDir(basePath)

	allowToContinue := false

	for _, item := range items {
		if item.IsDir() {
			relativePath := path.Join(basePath, item.Name())
			subUrls := GetArtifactUploadJobsFromPath(nexusRepoUrl, relativePath, urlContinueMask, logger)
			urls = append(urls, subUrls...)

		} else {
			nexusUrl := strings.TrimRight(nexusRepoUrl, "/")
			url := fmt.Sprintf("%s/%s/%s", nexusUrl, basePath, item.Name())
			url = strings.ReplaceAll(url, maven.ArtifactsDirectory+"/", "")

			logger.Printf("url '%s'..\n", url)

			if !allowToContinue && len(urlContinueMask) > 0 {
				if strings.Contains(url, urlContinueMask) {
					allowToContinue = true
				}

			} else {
				fileRelativePath := path.Join(basePath, item.Name())

				uploadJob := FileUploadJob{
					FilePath: fileRelativePath,
					Url:      url,
				}

				urls = append(urls, uploadJob)
			}
		}
	}

	return urls
}
