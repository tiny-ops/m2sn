# Maven To Sonatype Nexus

## How to use

Prepare config:

```shell
cp config.yml-dist config.yml
```

Then use:

```shell
# Import artifacts from maven web repo then upload to nexus repository
./m2sn import

# Download artifacts from maven web repo
./m2sn download

# Upload artifacts from `artifacts` directory to nexus repository
./m2sn upload
```

## Continue upload

You can continue upload process from url mask.

```shell
./m2sn upload --continue-from-url="com/company/someapp/3.8.1/app-3.8.1"

./m2sn upload --continue-from-url="app-3.8.1"
```