package main

import (
	"github.com/urfave/cli/v2"
	"log"
	"m2sn/cmd"
	"m2sn/config"
	"os"
)

func main() {
	file, err := os.OpenFile("m2sn.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0755)

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	logger := log.New(file, "", log.LstdFlags)
	logger.Println("--------------------------------------------")
	logger.Printf("m2sn v%s build %s\n", AppVersion, AppBuild)

	appConfig, err := config.LoadConfigFromFile("config.yml", logger)

	if err != nil {
		log.Fatalf("error: %s\n", err.Error())
	}

	app := &cli.App{
		Commands: []*cli.Command{
			cmd.GetDownloadCommand(appConfig, logger),
			cmd.GetUploadCommand(appConfig, logger),
			cmd.GetImportCommand(appConfig, logger),
			cmd.GetFetchMavenUrlsCommand(appConfig, logger),
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
