package cache

import (
	"errors"
	"os"
	"strings"
)

const MavenUrlCacheFileName = "maven-urls.cache"

func CreateMavenUrlCache(cacheFile string, urls []string) error {

	f, err := os.Create(cacheFile)

	if err != nil {
		return err
	}

	defer f.Close()

	content := strings.Join(urls, "\n")

	_, err = f.WriteString(content)

	if err != nil {
		return err
	}

	return nil
}

func LoadMavenUrlsFromCache(cacheFile string) ([]string, error) {
	urls := make([]string, 0)

	if _, err := os.Stat(cacheFile); err == nil {
		bytes, err := os.ReadFile(cacheFile)

		if err != nil {
			return urls, err
		}

		content := string(bytes)

		urls = strings.Split(content, "\n")

	} else if errors.Is(err, os.ErrNotExist) {
		return urls, nil

	} else {
		return urls, errors.New("unable to read maven urls from file cache")
	}

	return urls, nil
}
