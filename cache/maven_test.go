package cache

import (
	"os"
	"testing"
)

func TestCreateAndLoadMavenUrlCache(t *testing.T) {
	urls := make([]string, 0)
	urls = append(urls, "https://github.com")
	urls = append(urls, "https://google.com")

	err := CreateMavenUrlCache(MavenUrlCacheFileName, urls)

	if err != nil {
		t.Fatalf("cache save error: %s", err.Error())
	}

	results, err := LoadMavenUrlsFromCache(MavenUrlCacheFileName)

	if err != nil {
		t.Fatalf("cache load error: %s", err.Error())
	}

	if len(results) != 2 {
		t.Fatalf("unexpected urls amount %d, expected 2", len(results))
	}
}

func TestLoadMavenUrlCache_Return_Empty_Array_For_Missing_Cache(t *testing.T) {
	_ = os.Remove(MavenUrlCacheFileName)

	results, err := LoadMavenUrlsFromCache(MavenUrlCacheFileName)

	if err != nil {
		t.Fatalf("cache load error: %s", err.Error())
	}

	if len(results) != 0 {
		t.Fatalf("unexpected urls amount %d, expected 0", len(results))
	}
}
