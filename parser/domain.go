package parser

type MavenRepositoryResource struct {
	Name string
	Url  string
}
