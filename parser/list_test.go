package parser

import (
	"log"
	"os"
	"strings"
	"testing"
)

func TestGetResourceListFromHtml(t *testing.T) {
	logger := log.New(os.Stderr, "", log.LstdFlags)

	fileContent, err := os.ReadFile("../test-data/list.html")

	if err != nil {
		t.Fatalf(err.Error())
	}

	baseUrl := "https://whatever.com"

	fileMaskIgnoreList := make([]string, 0)
	fileMaskIgnoreList = append(fileMaskIgnoreList, ".xml")
	fileMaskIgnoreList = append(fileMaskIgnoreList, ".md5")
	fileMaskIgnoreList = append(fileMaskIgnoreList, ".sha1")
	fileMaskIgnoreList = append(fileMaskIgnoreList, "SNAPSHOT")

	results, err := GetResourceListFromHtml(baseUrl, string(fileContent), fileMaskIgnoreList, logger)

	if err != nil {
		t.Fatalf(err.Error())
	}

	if len(results) != 6 {
		t.Fatalf("unexpected urls amount: %d, expected 6", len(results))
	}

	for i := range results {
		result := results[i]

		if result.Name == "../" {
			t.Fatalf("unexpected resource name '%s'", result.Name)
		}

		if !strings.HasPrefix(result.Url, baseUrl) {
			t.Fatalf("unexpected url, it should be start from prefix '%s', got: '%s'", baseUrl, result.Url)
		}
	}
}
