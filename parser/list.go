package parser

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"strings"
)

func GetResourceListFromHtml(baseUrl string, html string, fileMaskIgnoreList []string, logger *log.Logger) ([]MavenRepositoryResource, error) {
	resources := make([]MavenRepositoryResource, 0)

	htmlBody := strings.NewReader(html)

	doc, err := goquery.NewDocumentFromReader(htmlBody)

	if err != nil {
		logger.Println(err)
		return resources, err
	}

	baseUrlWithoutSlash := strings.TrimRight(baseUrl, "/")

	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		skip := false

		href, exists := s.Attr("href")

		if exists {
			name := s.Text()

			if name != "../" {
				if strings.HasPrefix(href, "http") && !strings.HasPrefix(href, baseUrl) {
					logger.Printf("possible external url '%s', skip\n", href)
					skip = true
				}

				for fm := range fileMaskIgnoreList {
					fileMask := fileMaskIgnoreList[fm]

					if strings.Contains(href, fileMask) {
						skip = true
					}
				}

				if !skip {
					url := fmt.Sprintf("%s/%s", baseUrlWithoutSlash, href)

					resource := MavenRepositoryResource{
						Name: name,
						Url:  url,
					}

					resources = append(resources, resource)
					logger.Println("+ url:", url)
				}
			}
		}
	})

	return resources, nil
}
