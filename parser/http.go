package parser

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func GetUrlContent(url string, logger *log.Logger) (string, error) {
	logger.Printf("getting content from url '%s'..\n", url)
	resp, err := http.Get(url)

	if err != nil {
		logger.Println("http get error:", err.Error())
		return "", err
	}

	defer resp.Body.Close()

	html, err := io.ReadAll(resp.Body)

	if err != nil {
		logger.Println("read response body error:", err.Error())
		return "", err
	}

	return fmt.Sprintf("%s", html), nil
}
