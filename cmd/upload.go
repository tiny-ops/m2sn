package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"m2sn/config"
	"m2sn/maven"
	"m2sn/nexus"
	"net/http"
	"os"
)

func GetUploadCommand(appConfig config.AppConfig, logger *log.Logger) *cli.Command {
	return &cli.Command{
		Name:    "upload",
		Aliases: []string{"u"},
		Usage:   fmt.Sprintf("upload artifacts from '%s' directory to nexus repository", maven.ArtifactsDirectory),
		Flags: []cli.Flag{
			GetContinueFromUrlMaskFlag(),
		},
		Action: func(cCtx *cli.Context) error {
			urlContinueMask := cCtx.String(ContinueFromUrlMaskFlag)

			logger.Printf("uploading artifacts from '%s' directory to nexus repository '%s'\n", maven.ArtifactsDirectory, appConfig.NexusConfig.RepositoryUrl)

			fileUploadJobs := nexus.GetArtifactUploadJobsFromPath(appConfig.NexusConfig.RepositoryUrl, maven.ArtifactsDirectory, urlContinueMask, logger)

			client := &http.Client{}

			for i := range fileUploadJobs {
				job := fileUploadJobs[i]

				err := nexus.UploadArtifact(client, job.FilePath, job.Url, appConfig.NexusConfig.Username, appConfig.NexusConfig.Password, logger)

				if err != nil {
					logger.Printf("upload error: %s\n", err.Error())
					os.Exit(1)
				}
			}

			logger.Println("---")
			logger.Println("complete")

			return nil
		},
	}
}
