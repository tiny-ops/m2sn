package cmd

import "github.com/urfave/cli/v2"

const ContinueFromUrlMaskFlag = "continue-from-url"

func GetContinueFromUrlMaskFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:        ContinueFromUrlMaskFlag,
		Usage:       "Continue process from url mask. Example: logback-3.7.2.pom",
		DefaultText: "",
		Value:       "",
		Required:    false,
	}
}
