package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"m2sn/cache"
	"m2sn/config"
	"m2sn/maven"
	"m2sn/nexus"
	"net/http"
	"os"
)

func GetImportCommand(appConfig config.AppConfig, logger *log.Logger) *cli.Command {
	return &cli.Command{
		Name:    "import",
		Aliases: []string{"i"},
		Usage:   "import artifacts from maven http repository to sonatype nexus",
		Flags: []cli.Flag{
			GetContinueFromUrlMaskFlag(),
		},
		Action: func(cCtx *cli.Context) error {
			urlContinueMask := cCtx.String(ContinueFromUrlMaskFlag)

			logger.Printf("importing artifacts from maven repo '%s'\n", appConfig.MavenUrl)

			artifactUrls, err := cache.LoadMavenUrlsFromCache(cache.MavenUrlCacheFileName)

			if err != nil {
				fmt.Printf("cache error: %s\n", err.Error())
				os.Exit(1)
			}

			if len(artifactUrls) > 0 {
				logger.Printf("reusing maven urls cache from file '%s' (remove file if you want to fetch urls again)", cache.MavenUrlCacheFileName)

			} else {
				artifactUrls, err = maven.GetArtifactsListFromMaven(appConfig.MavenUrl, appConfig.ImportConfig.IgnoreList, logger)

				if err != nil {
					fmt.Printf("error: %s\n", err.Error())
					os.Exit(1)
				}

				err = cache.CreateMavenUrlCache(cache.MavenUrlCacheFileName, artifactUrls)

				if err != nil {
					fmt.Printf("cache error: %s\n", err.Error())
					os.Exit(1)
				}
			}

			logger.Printf("downloading artifacts (%d)..\n", len(artifactUrls))

			err = maven.DownloadArtifacts(appConfig.MavenUrl, artifactUrls, logger)

			if err != nil {
				fmt.Printf("download error: %s\n", err.Error())
				os.Exit(1)
			}

			fileUploadJobs := nexus.GetArtifactUploadJobsFromPath(appConfig.NexusConfig.RepositoryUrl, maven.ArtifactsDirectory, urlContinueMask, logger)

			client := &http.Client{}

			for i := range fileUploadJobs {
				job := fileUploadJobs[i]

				err := nexus.UploadArtifact(client, job.FilePath, job.Url, appConfig.NexusConfig.Username, appConfig.NexusConfig.Password, logger)

				if err != nil {
					logger.Printf("upload error: %s\n", err.Error())
					os.Exit(1)
				}
			}

			logger.Println("---")
			logger.Println("complete")

			return nil
		},
	}
}
