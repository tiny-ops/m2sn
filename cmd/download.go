package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"m2sn/cache"
	"m2sn/config"
	"m2sn/maven"
	"os"
)

func GetDownloadCommand(appConfig config.AppConfig, logger *log.Logger) *cli.Command {
	return &cli.Command{
		Name:    "download",
		Aliases: []string{"d"},
		Usage:   fmt.Sprintf("download artifacts maven web repository to '%s' directory", maven.ArtifactsDirectory),
		Flags:   []cli.Flag{},
		Action: func(cCtx *cli.Context) error {
			logger.Printf("downloading artifacts from '%s' directory from maven repository '%s'\n", maven.ArtifactsDirectory, appConfig.MavenUrl)

			artifactUrls, err := cache.LoadMavenUrlsFromCache(cache.MavenUrlCacheFileName)

			if err != nil {
				fmt.Printf("cache error: %s\n", err.Error())
				os.Exit(1)
			}

			if len(artifactUrls) > 0 {
				logger.Printf("reusing maven urls cache from file '%s' (remove file if you want to fetch urls again)", cache.MavenUrlCacheFileName)

			} else {
				artifactUrls, err = maven.GetArtifactsListFromMaven(appConfig.MavenUrl, appConfig.ImportConfig.IgnoreList, logger)

				if err != nil {
					fmt.Printf("error: %s\n", err.Error())
					os.Exit(1)
				}

				err = cache.CreateMavenUrlCache(cache.MavenUrlCacheFileName, artifactUrls)

				if err != nil {
					fmt.Printf("cache error: %s\n", err.Error())
					os.Exit(1)
				}
			}

			logger.Printf("downloading artifacts (%d)..\n", len(artifactUrls))

			err = maven.DownloadArtifacts(appConfig.MavenUrl, artifactUrls, logger)

			if err != nil {
				fmt.Printf("download error: %s\n", err.Error())
				os.Exit(1)
			}

			logger.Println("---")
			logger.Println("complete")

			return nil
		},
	}
}
