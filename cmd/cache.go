package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"m2sn/cache"
	"m2sn/config"
	"m2sn/maven"
	"os"
)

func GetFetchMavenUrlsCommand(appConfig config.AppConfig, logger *log.Logger) *cli.Command {
	return &cli.Command{
		Name:    "fetch-maven-urls",
		Aliases: []string{"fmu"},
		Usage:   fmt.Sprintf("fetch maven artifact urls and create file cache '%s'", cache.MavenUrlCacheFileName),
		Flags:   []cli.Flag{},
		Action: func(cCtx *cli.Context) error {
			logger.Printf("fetching maven urls from '%s'..\n", appConfig.MavenUrl)

			artifactUrls, err := maven.GetArtifactsListFromMaven(appConfig.MavenUrl, appConfig.ImportConfig.IgnoreList, logger)

			if err != nil {
				fmt.Printf("error: %s\n", err.Error())
				os.Exit(1)
			}

			err = cache.CreateMavenUrlCache(cache.MavenUrlCacheFileName, artifactUrls)

			if err != nil {
				fmt.Printf("cache error: %s\n", err.Error())
				os.Exit(1)
			}

			logger.Println("---")
			logger.Println("complete")

			return nil
		},
	}
}
