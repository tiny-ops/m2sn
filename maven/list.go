package maven

import (
	"log"
	"m2sn/parser"
	"strings"
)

func GetArtifactsListFromMaven(mavenBaseUrl string, fileMaskIgnoreList []string, logger *log.Logger) ([]string, error) {
	logger.Printf("getting maven artifacts from '%s'..\n", mavenBaseUrl)

	results := make([]string, 0)

	pageHtml, err := parser.GetUrlContent(mavenBaseUrl, logger)

	if err != nil {
		logger.Printf("unable to get url '%s' content: %s\n", mavenBaseUrl, err.Error())
		return results, err
	}

	resources, err := parser.GetResourceListFromHtml(mavenBaseUrl, pageHtml, fileMaskIgnoreList, logger)

	if err != nil {
		logger.Printf("unable to get resource list from url '%s' content: %s\n", mavenBaseUrl, err.Error())
		return results, err
	}

	for i := range resources {
		resource := resources[i]
		logger.Printf("resource: %+v\n", resource)

		if strings.HasSuffix(resource.Url, "/") {
			logger.Println("type: directory")
			urlResources, err := GetArtifactsListFromMaven(resource.Url, fileMaskIgnoreList, logger)

			if err != nil {
				logger.Println(err.Error())
				return results, err
			}

			results = append(results, urlResources...)

		} else {
			logger.Println("type: artifact")

			results = append(results, resource.Url)
		}
	}

	return results, nil
}
