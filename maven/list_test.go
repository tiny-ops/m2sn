package maven

import (
	"log"
	"os"
	"strings"
	"testing"
)

// Integration test
func TestGetArtifactsListFromMaven(t *testing.T) {
	t.Skip()

	logger := log.New(os.Stderr, "", log.LstdFlags)

	fileMaskIgnoreList := make([]string, 0)
	fileMaskIgnoreList = append(fileMaskIgnoreList, ".xml")
	fileMaskIgnoreList = append(fileMaskIgnoreList, ".md5")
	fileMaskIgnoreList = append(fileMaskIgnoreList, ".sha1")
	fileMaskIgnoreList = append(fileMaskIgnoreList, "SNAPSHOT")

	artifactUrls, err := GetArtifactsListFromMaven("CHANGE-ME", fileMaskIgnoreList, logger)

	if err != nil {
		t.Fatal(err.Error())
	}

	if len(artifactUrls) == 0 {
		t.Fatalf("no artifact urls found")
	}

	for i := range artifactUrls {
		url := artifactUrls[i]

		if strings.HasSuffix(url, "/") {
			t.Fatalf("unexpected url type (directory): '%s'", url)
		}

		for f := range fileMaskIgnoreList {
			mask := fileMaskIgnoreList[f]

			if strings.Contains(url, mask) {
				t.Fatalf("ignore file mask list doesn't work. url '%s' present in results instead of filter mask '%s'", url, mask)
			}
		}
	}
}

func TestGetRelativeArtifactPathFromUrl(t *testing.T) {
	result := GetRelativeArtifactPathFromUrl("https://repos.company.com/repository/maven2", "https://repos.company.com/repository/maven2/com/tinyops/app/pw-1.0.0.jar")

	if result != "com/tinyops/app" {
		t.Fatalf("unexpected result path '%s', expected: 'com/tinyops/app'", result)
	}
}
