package maven

import (
	"log"
	"os"
	"strings"
	"testing"
)

func TestGetArtifactsFromPath(t *testing.T) {
	logger := log.New(os.Stderr, "", log.LstdFlags)

	results, err := GetArtifactsFromPath("../test-data/artifacts", logger)

	if err != nil {
		t.Fatalf("unexpected error: %s", err.Error())
	}

	if len(results) != 3 {
		t.Fatalf("unexpected results amount: %d, expected 3", len(results))
	}

	for r := range results {
		artifact := results[r]

		if len(artifact.Files) == 0 {
			t.Fatalf("unexpected Files amount for artifact '%s': %d, expected > 0", artifact.Name, len(artifact.Files))
		}

		for i := range artifact.Files {
			fileName := artifact.Files[i]

			if !strings.HasPrefix(fileName, artifact.Name) {
				t.Fatalf("artifact file prefix '%s' doesn't match with artifact name '%s'", fileName, artifact.Name)
			}
		}
	}
}
