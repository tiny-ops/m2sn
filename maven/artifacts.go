package maven

import (
	"log"
	"os"
	"path"
	"regexp"
)

type Artifact struct {
	Name  string
	Files []string
}

func GetArtifactsFromPath(filePath string, logger *log.Logger) ([]Artifact, error) {
	logger.Printf("collecting artifact Files from filePath '%s'..\n", filePath)

	artifacts := make([]Artifact, 0)

	artifactFileMap := make(map[string][]string)

	items, _ := os.ReadDir(filePath)

	for _, item := range items {
		if !item.IsDir() {
			re := regexp.MustCompile("([a-zA-Z0-9\\-]+)-\\d+\\.\\d+\\.\\d+(-[a-z]+)?\\.\\w+")

			matches := re.FindStringSubmatch(item.Name())

			if len(matches) >= 2 {
				artifactName := matches[1]
				files, ok := artifactFileMap[artifactName]

				if !ok {
					files = make([]string, 0)
				}

				files = append(files, item.Name())

				artifactFileMap[artifactName] = files

			} else {
				logger.Printf("file doesn't match pattern '%s', skip\n", item.Name())
			}

		} else {
			nextPath := path.Join(filePath, item.Name())
			nextArtifacts, err := GetArtifactsFromPath(nextPath, logger)

			if err != nil {
				return artifacts, err
			}

			artifacts = append(artifacts, nextArtifacts...)
		}
	}

	for key := range artifactFileMap {
		files, _ := artifactFileMap[key]

		artifact := Artifact{
			Name:  key,
			Files: files,
		}

		artifacts = append(artifacts, artifact)
	}

	return artifacts, nil
}
