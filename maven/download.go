package maven

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strings"
)

const ArtifactsDirectory = "artifacts"

func DownloadArtifacts(repositoryBaseUrl string, urls []string, logger *log.Logger) error {
	logger.Printf("downloading artifacts from maven repo '%s'..\n", repositoryBaseUrl)

	_ = os.Mkdir(ArtifactsDirectory, os.FileMode(0750))

	for i := range urls {
		url := urls[i]

		relativeDirPath := GetRelativeArtifactPathFromUrl(repositoryBaseUrl, url)
		logger.Printf("relative artifact directory path '%s'\n", relativeDirPath)

		err := os.MkdirAll(path.Join(ArtifactsDirectory, relativeDirPath), os.FileMode(0750))

		if err != nil {
			logger.Println(err.Error())
			return err
		}

		relativeFilePath := fmt.Sprintf("%s/%s/%s", ArtifactsDirectory, relativeDirPath, path.Base(url))

		logger.Printf("relative artifact file path '%s'\n", relativeFilePath)

		if _, err := os.Stat(relativeFilePath); os.IsNotExist(err) {

			logger.Printf("downloading file from url '%s' to '%s'..\n", url, relativeFilePath)
			err := DownloadFile(url, relativeFilePath)

			if err != nil {
				logger.Printf("unable to download file '%s': %s", url, err.Error())
				return err
			}

		} else {
			logger.Printf("artifact '%s' already downloaded, skip\n", relativeFilePath)
		}
	}

	return nil
}

func DownloadFile(url string, filepath string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	out, err := os.Create(filepath)

	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)

	return err
}

func GetRelativeArtifactPathFromUrl(repositoryBaseUrl string, url string) string {
	relativeFilePath := strings.ReplaceAll(url, repositoryBaseUrl, "")
	relativeFilePath = strings.TrimLeft(relativeFilePath, "/")
	return path.Dir(relativeFilePath)
}
