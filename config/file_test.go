package config

import (
	"log"
	"os"
	"reflect"
	"testing"
)

func TestLoadConfigFromFile(t *testing.T) {
	logger := log.New(os.Stderr, "", log.LstdFlags)

	config, err := LoadConfigFromFile("../config.yml-dist", logger)

	if err != nil {
		t.Fatalf("Error: %s", err)
	}

	expectedIgnoreList := make([]string, 0)
	expectedIgnoreList = append(expectedIgnoreList, "SNAPSHOT")
	expectedIgnoreList = append(expectedIgnoreList, ".md5")
	expectedIgnoreList = append(expectedIgnoreList, ".sha1")
	expectedIgnoreList = append(expectedIgnoreList, ".xml")
	expectedIgnoreList = append(expectedIgnoreList, ".asc")
	expectedIgnoreList = append(expectedIgnoreList, "_maven.repositories")

	expectedConfig := AppConfig{
		MavenUrl: "https://repos.company.com/maven2",
		NexusConfig: NexusConfig{
			RepositoryUrl: "https://nexus.company.com/repository/maven2/",
			Username:      "CHANGE-ME",
			Password:      "CHANGE-ME",
		},
		ImportConfig: ImportConfig{
			IgnoreList: expectedIgnoreList,
		},
	}

	if !reflect.DeepEqual(expectedConfig, config) {
		t.Fatalf("Unexpected config data:\nexpected: %+v\nactual:%+v", expectedConfig, config)
	}
}
