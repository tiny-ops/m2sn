package config

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

func LoadConfigFromFile(fileName string, logger *log.Logger) (AppConfig, error) {
	logger.Printf("loading config from file '%s'..\n", fileName)

	fileContent, err := os.ReadFile(fileName)
	if err != nil {
		logger.Fatalf("config read error: %s\n", err.Error())
		return AppConfig{}, err
	}

	var appConfig AppConfig
	err = yaml.Unmarshal(fileContent, &appConfig)

	if err != nil {
		logger.Fatalf("config load error: %v", err)
	}

	logger.Printf("loaded config: %+v\n", appConfig)
	return appConfig, nil
}
