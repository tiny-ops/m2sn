package config

type AppConfig struct {
	MavenUrl string `yaml:"maven-url"`

	NexusConfig  NexusConfig  `yaml:"nexus"`
	ImportConfig ImportConfig `yaml:"import"`
}

type NexusConfig struct {
	RepositoryUrl string `yaml:"repository-url"`
	Username      string `yaml:"username"`
	Password      string `yaml:"password"`
}

type ImportConfig struct {
	IgnoreList []string `yaml:"ignore-list"`
}
